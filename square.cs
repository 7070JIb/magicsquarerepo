﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text;
namespace ConsoleApp1
{
    class Square
    {
        static void Main()
        {
            int[,] square = null;
            string[] commands;
            Console.WriteLine("Постройка магического квадрата любого порядка");
            do
            {
                Console.WriteLine("\nСписок команд:");
                Console.WriteLine("\"build n\" - (n - целое число). Построить магический квадрат порядка n");
                Console.WriteLine("\"display\" - отобразить квадрат в консоли");
                Console.WriteLine("\"check\" - проверить квадрат");
                Console.WriteLine("\"save\" - сохранить в формате TSV");
                Console.WriteLine("\"exit\" - выйти из программы");
                Console.WriteLine();
                Console.Write("->");
                commands = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                Console.Clear();
                switch(commands[0])
                {
                    case "build":
                        {
                            if(int.TryParse(commands[1], out int n))
                            {
                                if(n%4 == 0 )
                                {
                                    square = Build4(n);
                                }
                                else if(n%2==0)
                                {
                                    square = Build2(n);
                                }
                                else
                                {
                                    square = BuildNechet(n);
                                }
                                Console.WriteLine("Магический квадрат " + n + "x" + n + " построен");
                            }
                            else
                            {
                                goto default;
                            }
                        }
                        break;
                    case "display":
                        {
                            if(square != null)
                            {
                                Display(square);
                            }
                            else
                            {
                                Console.WriteLine("Квадрат еще не создан");
                                goto default;
                            }
                        }
                        break;
                    case "check":
                        {
                            if(square != null)
                            {
                                IsValid(square);
                            }
                            else
                            {
                                Console.WriteLine("Квадрат еще не создан");
                                goto default;
                            }
                        }
                        break;
                    case "save":
                        {
                            if(square != null)
                            {
                                SaveAsTSV(square);
                            }
                            else
                            {
                                Console.WriteLine("Квадрат еще не создан");
                                goto default;
                            }
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Неправильная команда...");
                        }
                        break;
                }
            }
            while (commands[0] != "exit");
        }

        private static void SaveAsTSV(int[,] square)
        {
            StreamWriter stream = new StreamWriter(new FileStream("square" + square.GetLength(0) + "x" + square.GetLength(0) + ".tsv", FileMode.Create));
            for(int y = 0; y < square.GetLength(0); y++)
            {
                for(int x = 0;x < square.GetLength(1);x++)
                {
                    stream.Write(square[y, x].ToString() + '\t') ;
                }
                stream.Write('\n');
            }
            stream.Dispose();
            stream.Close();
        }
        //Что-то написал
        public static void Display(int[,] square)
        {
            Console.WriteLine("\nОтобразить квадрат:");
            for (int y = 0; y < square.GetLength(0); y++)
            {
                for (int x = 0; x < square.GetLength(1); x++)
                {
                    
                    Console.Write( square[y, x] + " \t");
                }
                Console.WriteLine();
            }
        }
        public static void IsValid(int[,] square)
        {
            Console.WriteLine("\nПроверка на правильность:");

            int[] sumXar = new int[square.GetLength(0)].Select(t => 0).ToArray();
            int[] sumYar = new int[square.GetLength(0)].Select(t => 0).ToArray();
            for (int y = 0; y < square.GetLength(0); y++)
            {

                for (int x = 0; x < square.GetLength(0); x++)
                {
                    Console.Write(square[y, x] + " \t");
                    sumXar[y] += square[y, x];
                    sumYar[x] += square[y, x];
                }
                Console.Write("= " + sumXar[y]);
                Console.WriteLine();
            }
            for (int i = 0; i < square.GetLength(0); i++)
            {
                Console.Write("= \t");
            }
            Console.WriteLine();
            for (int i = 0; i < square.GetLength(0); i++)
            {
                Console.Write(sumYar[i] + " \t");
            }
            Console.WriteLine();
            if (sumYar.All(t => t == sumYar.First()) && sumXar.All(t => t == sumXar.First()))
            {
                Console.WriteLine("Квадрат прошел проверку на правильность");
            }
            else
            {
                Console.WriteLine("Квадрат НЕ прошел проверку на правильность");
            }
        }
        public static int[,] BuildMatrix(int n)
        {
            int[,] ar = new int[n, n];
            for (int i = 1; i <= n * n; i++)
            {
                int x = (i - 1) % n;
                int y = (i - 1) / n;
                ar[y, x] = i;
            }
            return ar;
        }
        public static int[,] BuildNechet(int n)
        {
            int[,] arInit = BuildMatrix(n);

            int[,] arRes = new int[n, n];
            for (int y = 0; y < n; y++)
            {
                for (int x = 0; x < n; x++)
                {
                    //1 угол
                    if (x + y < (n - 1) / 2)
                    {
                        arRes[(n - 1) / 2 - x + y, (n - 1) / 2 + x + y + 1] = arInit[y, x];
                    }
                    //2 угол
                    else if (x - y > (n - 1) / 2)
                    {
                        arRes[n + (n - 1) / 2 - x + y, x + y - (n - 1) / 2] = arInit[y, x];
                    }
                    //3 угол
                    else if (x + y >= n + (n - 1) / 2)
                    {
                        //Console.WriteLine("В 3 углу значение " + arInter[y, x] + " c координатами " + y + ";" + x + " переносится в координаты " + ((n - 1) / 2 + x - y) + ";" + (x + y - n - (n - 1) / 2));
                        arRes[(n - 1) / 2 - x + y, x + y - n - (n - 1) / 2] = arInit[y, x];
                    }
                    //4 угол
                    else if (y - x > (n - 1) / 2)
                    {
                        arRes[y - x - (n - 1) / 2 - 1, x + y - (n - 1) / 2] = arInit[y, x];
                    }
                    //Внутри матрицы
                    else
                    {
                        arRes[(n - 1) / 2 - x + y, x + y - (n - 1) / 2] = arInit[y, x];
                    }
                }
            }

            return arRes;
        }
        public static int[,] Build4(int n)
        {
            int[,] interMatrix = new int[2*n - 2, n];
            (int y,int x, int i, int invertVal) = (n / 2 - 1,0, 1, 1);

            for (int k = 0; k < n / 2; k++)
            {
                for (int j = 0; j < n / 2 - 1; j++)
                {
                    interMatrix[y, x] = i++;
                    y--; x += 1 * invertVal;
                }
                interMatrix[y, x] = i++;
                x += 1 * invertVal;

                for (int j = 0; j < n / 2 - 1; j++)
                {
                    interMatrix[y, x] = i++;
                    x += 1 * invertVal;
                    y++;
                }

                interMatrix[y, x] = i++;
                y++;

                for (int j = 0; j < n / 2 - 1; j++)
                {
                    interMatrix[y, x] = i++;
                    x -= 1 * invertVal;
                    y++;
                }

                interMatrix[y, x] = i++;
                x -= 1 * invertVal;
                for (int j = 0; j < n / 2 - 1; j++)
                {
                    interMatrix[y, x] = i++;
                    x -= 1 * invertVal;
                    y--;
                }
                interMatrix[y, x] = i++;
                x = (invertVal == 1) ? n - 1 : 0;
                y++;
                invertVal = -invertVal;
            }
            int[,] newMatrix = new int[n, n];
            for ( y = 0; y < interMatrix.GetLength(0); y++)
            {
                for ( x = 0; x < interMatrix.GetLength(1); x++)
                {
                    int _y;
                    if(interMatrix[y,x]!=0)
                    {
                        if(y < n/2-1)
                        {
                            _y = y + n - (n/2-1);
                        }
                        else if(y >= n + n / 2 - 1)
                        {
                            _y = y - n - (n/2-1);
                        }
                        else
                        {
                            _y = y -(n / 2 - 1);
                        }
                        newMatrix[_y, x] = interMatrix[y, x];
                    }
                }
            }
            return newMatrix;
        }

        public static int[,] Build2(int n)
        {
            int[,] matrixFull = new int[n, n];
            int[,] matrixNW = BuildNechet(n / 2);
            for (int y = 0; y < matrixNW.GetLength(0); y++)
            {
                for (int x = 0; x < matrixNW.GetLength(1); x++)
                {
                    matrixFull[y, x] = matrixNW[y, x];
                    matrixFull[y, x + n / 2] = matrixNW[y, x] + (int)Math.Pow((n / 2), 2) * 2;
                    matrixFull[y + n / 2, x + n / 2] = matrixNW[y, x] + (int)Math.Pow((n / 2), 2);
                    matrixFull[y + n / 2, x] = matrixNW[y, x] + (int)Math.Pow((n / 2), 2) * 3;
                }
            }
            ReplaceVals(matrixFull, 0, 0, n/2, 0);
            ReplaceVals(matrixFull, n/2-1, 0, n-1, 0);

            for (int y = 1; y < n/2-1;y++)
            {
                ReplaceVals(matrixFull, y, 1, y + n / 2, 1);
            }

            for(int x = 4; x < n-4; x++)
            {
                for(int y = 0; y < n/2; y++)
                {
                    ReplaceVals(matrixFull, y, x, y + n / 2, x);
                }
            }

            return matrixFull;
        }
            
        public static void ReplaceVals(int[,] square, int y1, int x1, int y2, int x2)
        {
            int val = square[y1, x1];
            square[y1, x1] = square[y2, x2];
            square[y2, x2] = val;
        }
    }
}
